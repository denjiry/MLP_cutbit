import numpy as np


def as_fixed(x, f):
    if f == "fp":
        return x
    db = 2 ** f
    # raund to 0
    xi = np.array(x * db, dtype=np.int)
    return (xi / db).astype(np.float32)
