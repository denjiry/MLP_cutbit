# coding: utf-8
import chainer
import numpy as np

a = np.array([2], dtype=np.float32)
x = chainer.Variable(a)
y = 2*x**2 - 7*x
y.backward()
print(x.grad)
