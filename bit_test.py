import pytest
from bit import as_fixed
import math
import numpy as np


def test_as_fixed():
    x = math.pi
    X = np.array([[[0., 0.125],
                   [0.25, 0.375]],

                  [[0.5, 0.625],
                   [0.75, 0.875]]])
    f = 5
    F = 2
    ret = as_fixed(x, f)
    RET = as_fixed(X, F)
    ans = 3.125
    ANS = np.array([[[0., 0.],
                     [0.25, 0.25]],

                    [[0.5, 0.5],
                     [0.75, 0.75]]])
    err = 1/(2**f)
    ERR = 1/(2**F)
    assert as_fixed(x, "fp") is x
    assert np.allclose(ans, ret, rtol=0, atol=err)
    assert np.allclose(ANS, RET, rtol=0, atol=ERR)
    return ret, RET, err
