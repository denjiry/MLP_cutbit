import numpy

from chainer import cuda
from chainer import function
from chainer.utils import type_check

from bit import as_fixed

def _kern():
    return cuda.elementwise(
        'T x', 'T y',
        'y = x >= 0 ? 1 : -1',
        'binarize')

def _as_mat(x):
    if x.ndim == 2:
        return x
    return x.reshape(len(x), -1)



class BinaryLinearFunction(function.Function):
    in_bw = "fp"
    w_bw = "fp"
    clip = 1

    def check_type_forward(self, in_types):
        n_in = in_types.size()
        type_check.expect(2 <= n_in, n_in <= 3)
        x_type, w_type = in_types[:2]

        type_check.expect(
            x_type.dtype == numpy.float32,
            w_type.dtype == numpy.float32,
            x_type.ndim >= 2,
            w_type.ndim == 2,
            type_check.prod(x_type.shape[1:]) == w_type.shape[1],
        )
        if n_in == 3:
            b_type = in_types[2]
            type_check.expect(
                b_type.dtype == numpy.float32,
                b_type.ndim == 1,
                b_type.shape[0] == w_type.shape[0],
            )

    def forward_cpu(self, inputs):
        x = _as_mat(as_fixed(inputs[0], self.in_bw))
        W = as_fixed(inputs[1], self.w_bw)
        y = x.dot(W.T)
        if len(inputs) == 3:
            b = as_fixed(inputs[2], self.w_bw)
            y += b
        return y,

    def forward_gpu(self, inputs):
        x = _as_mat(inputs[0])
        W = inputs[1]
        Wb = _kern()(W)
        y = x.dot(Wb.T)
        if len(inputs) == 3:
            b = inputs[2]
            y += b
        return y,


    def backward_cpu(self, inputs, grad_outputs):
        x = _as_mat(as_fixed(inputs[0], self.in_bw))
        W = as_fixed(inputs[1], self.w_bw)
        gy = as_fixed(grad_outputs[0], self.w_bw)

        gx = gy.dot(W).reshape(inputs[0].shape)
        gW = gy.T.dot(x)
        if len(inputs) == 3:
            gb = gy.sum(0)
            return gx, gW, gb
        else:
            return gx, gW

    def backward_gpu(self, inputs, grad_outputs):
        x = _as_mat(inputs[0])
        W = inputs[1]
        Wb = _kern()(W)
        gy = grad_outputs[0]

        gx = gy.dot(Wb).reshape(inputs[0].shape)
        gW = gy.T.dot(x)
        if len(inputs) == 3:
            gb = gy.sum(0)
            return gx, gW, gb
        else:
            return gx, gW


def binary_linear(x, W, b, in_bw="fp", w_bw="fp", clip=True, bclip=None):
    """Binary Linear function, or affine transformation.

    It accepts two or three arguments: an input minibatch ``x``, a weight
    matrix ``W``, and optionally a bias vector ``b``. It computes
    :math:`Y = xW^\\top + b`.

    Args:
        x (~chainer.Variable): Input variable. Its first dimension is assumed
            to be the *minibatch dimension*. The other dimensions are treated
            as concatenated one dimension whose size must be ``N``.
        W (~chainer.Variable): Weight variable of shape ``(M, N)``.
        b (~chainer.Variable): Bias variable (optional) of shape ``(M,)``..

    Returns:
        ~chainer.Variable: Output variable.

    .. seealso:: :class:`~chainer.links.Linear`

    """
    blf = BinaryLinearFunction()
    blf.in_bw = in_bw
    blf.w_bw = w_bw
    # clip each W's element to 1/(1+W.shape[1]) e.g.[1/(1+F)]
    floor = numpy.array(1/(1+W.shape[1]), dtype=numpy.float32)
    W.data = numpy.clip(W.data, -floor, floor) if clip else W.data
    b.data = numpy.clip(b.data, -bclip, bclip) if bclip else b.data
    return blf(x, W, b)
