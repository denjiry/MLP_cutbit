#!/usr/bin/env python
import argparse
from pathlib import Path

import chainer
import chainer.functions as F
import chainer.links as L
from chainer import training
from chainer.training import extensions
import numpy as np

import net


def main(batchsize, epoch, frequency, gpu, out, resume, unit, plot,
         in_bw, w_bw, dsize):
    print('GPU: {}'.format(gpu))
    print('# unit: {}'.format(unit))
    print('# Minibatch-size: {}'.format(batchsize))
    print('# epoch: {}'.format(epoch))
    print('# in_bw: {}'.format(in_bw))
    print('# w_bw: {}'.format(w_bw))
    print('')

    # Set up a neural network to train
    # Classifier reports softmax cross entropy loss and accuracy at every
    # iteration, which will be used by the PrintReport extension below.
    if in_bw == -1:
        in_bw = "fp"
    if w_bw == -1:
        w_bw = "fp"
    NN = net.BinMLP(unit, 1, in_bw=in_bw, w_bw=w_bw)
    model = L.Classifier(NN, lossfun=F.mean_squared_error)
    model.compute_accuracy = False
    if gpu >= 0:
        # Make a specified GPU current
        chainer.backends.cuda.get_device_from_id(gpu).use()
        model.to_gpu()  # Copy the model to the GPU

    # Setup an optimizer
    optimizer = chainer.optimizers.Adam()
    optimizer.setup(model)

    # Load the MNIST dataset
    def data(dsize=6000, seed=0):
        np.random.RandomState(seed=seed)
        x = np.random.random_sample((dsize, 1)).astype(np.float32)
        y = np.sin(6*(x-0.5))
        return chainer.datasets.tuple_dataset.TupleDataset(x, y)

    train = data()
    test = data(dsize=dsize, seed=1)

    train_iter = chainer.iterators.SerialIterator(train, batchsize)
    test_iter = chainer.iterators.SerialIterator(test, batchsize,
                                                 repeat=False, shuffle=False)

    # Set up a trainer
    updater = training.updaters.StandardUpdater(
        train_iter, optimizer, device=gpu)
    trainer = training.Trainer(updater, (epoch, 'epoch'), out=out)

    # Evaluate the model with the test dataset for each epoch
    trainer.extend(extensions.Evaluator(test_iter, model, device=gpu))

    # Report Variable Statistics
    # trainer.extend(
    #     extensions.VariableStatisticsPlot(model.predictor.l1, 100,
    #                                       file_name="l1.png"))
    # trainer.extend(
    #     extensions.VariableStatisticsPlot(model.predictor.l1.b, 100,
    #                                       file_name="l1b.png"))
    # trainer.extend(
    #     extensions.VariableStatisticsPlot(model.predictor.l2, 100,
    #                                       file_name="l2.png"))
    # trainer.extend(
    #     extensions.VariableStatisticsPlot(model.predictor.l2.b, 100,
    #                                       file_name="l2b.png"))
    # trainer.extend(
    #     extensions.VariableStatisticsPlot(model.predictor.l3, 100,
    #                                       file_name="l3.png"))
    # trainer.extend(
    #     extensions.VariableStatisticsPlot(model.predictor.l3.b, 100,
    #                                       file_name="l3b.png"))

    # Dump a computational graph from 'loss' variable at the first iteration
    # The "main" refers to the target link of the "main" optimizer.
    trainer.extend(extensions.dump_graph('main/loss'))

    # Take a snapshot for each specified epoch
    frequency = epoch if frequency == -1 else max(1, frequency)
    trainer.extend(extensions.snapshot(), trigger=(frequency, 'epoch'))

    # Write a log of evaluation statistics for each epoch
    trainer.extend(extensions.LogReport())

    # Save two plot images to the result dir
    if plot and extensions.PlotReport.available():
        trainer.extend(
            extensions.PlotReport(['main/loss', 'validation/main/loss'],
                                  'epoch', file_name='loss.png'))
        trainer.extend(
            extensions.PlotReport(
                ['main/accuracy', 'validation/main/accuracy'],
                'epoch', file_name='accuracy.png'))

    # Print selected entries of the log to stdout
    # Here "main" refers to the target link of the "main" optimizer again, and
    # "validation" refers to the default name of the Evaluator extension.
    # Entries other than 'epoch' are reported by the Classifier link, called by
    # either the updater or the evaluator.
    trainer.extend(extensions.PrintReport(
        ['epoch', 'main/loss', 'validation/main/loss',
         'main/accuracy', 'validation/main/accuracy', 'elapsed_time']))

    # Print a progress bar to stdout
    trainer.extend(extensions.ProgressBar())

    if resume:
        # Resume from a snapshot
        chainer.serializers.load_npz(resume, trainer)

    # Run the training
    trainer.run()

    def view_model_as_graph(model, figpath):
        import pandas as pd
        import matplotlib.pyplot as plt
        dots = 6000
        x = (np.arange(dots, dtype=np.float32)/dots).reshape(dots, 1)
        with chainer.no_backprop_mode():
            y_pred = model.predictor(x).data
        y_true = np.sin(6*(x-0.5))
        df = pd.DataFrame.from_dict({"x": x.ravel(),
                                     "y_true": y_true.ravel(),
                                     "y_pred": y_pred.ravel()})
        df.set_index("x").plot()
        plt.savefig(figpath)
        return
    view_model_as_graph(model, str(Path(out) / Path("sin.png")))
    print("h1_max:", model.predictor.h1_max)
    print("h2_max:", model.predictor.h2_max)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Chainer example: MNIST')
    parser.add_argument('--batchsize', '-b', type=int, default=100,
                        help='Number of images in each mini-batch')
    parser.add_argument('--epoch', '-e', type=int, default=50,
                        help='Number of sweeps over the dataset to train')
    parser.add_argument('--frequency', '-f', type=int, default=-1,
                        help='Frequency of taking a snapshot')
    parser.add_argument('--gpu', '-g', type=int, default=-1,
                        help='GPU ID (negative value indicates CPU)')
    parser.add_argument('--out', '-o', default='result',
                        help='Directory to output the result')
    parser.add_argument('--resume', '-r', default='',
                        help='Resume the training from snapshot')
    parser.add_argument('--unit', '-u', type=int, default=10,
                        help='Number of units')
    parser.add_argument('--noplot', dest='plot', action='store_false',
                        help='Disable PlotReport extension')
    parser.add_argument('--in_bw', '-i', type=int,
                        nargs=3, default=[-1, -1, -1],
                        help='Input bit width')
    parser.add_argument('--w_bw', '-w', type=int,
                        nargs=3, default=[-1, -1, -1],
                        help='Weight bit width')
    parser.add_argument('--dsize', '-d', type=int, default=1000,
                        help='Data size')
    args = parser.parse_args()

    main(args.batchsize, args.epoch, args.frequency, args.gpu,
         args.out, args.resume, args.unit, args.plot,
         args.in_bw, args.w_bw, args.dsize)
