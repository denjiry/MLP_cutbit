import chainer
import chainer.functions as F
import chainer.links as L
import link_binary_linear


class BinMLP(chainer.Chain):
    h1_max = 0.0
    h2_max = 0.0

    """An example of multi-layer perceptron for MNIST dataset.

    """
    def __init__(self, n_units, n_out,
                 in_bw=["fp", "fp", "fp"], w_bw=["fp", "fp", "fp"]):
        super(BinMLP, self).__init__()
        with self.init_scope():
            self.l1 = link_binary_linear.BinaryLinear(
                None, n_units, in_bw=in_bw[0], w_bw=w_bw[0],
                clip=True, bclip=0.5)
            self.l2 = link_binary_linear.BinaryLinear(
                None, n_units, in_bw=in_bw[1], w_bw=w_bw[1],
                clip=False, bclip=None)
                # clip=True, bclip=1/(1+n_units))
            self.l3 = link_binary_linear.BinaryLinear(
                None, n_out, in_bw=in_bw[2], w_bw=w_bw[2],
                clip=False, bclip=None)

    def __call__(self, x):
        h1 = F.relu(self.l1(x))
        self.h1_max = max(h1.data.max(), self.h1_max)
        h2 = F.relu(self.l2(h1))
        self.h2_max = max(h2.data.max(), self.h2_max)
        return self.l3(h2)
